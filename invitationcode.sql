-- Table structure for table `invitecode`
--

CREATE TABLE IF NOT EXISTS `invitecode` (
  `codes` varchar(8) NOT NULL,
  `attending` varchar(7) NOT NULL DEFAULT 'unknown',
  `flag` int(1) NOT NULL,
  `name` varchar(30) NOT NULL,
  `numguests` int(1) NOT NULL,
  `date_rsvpd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
