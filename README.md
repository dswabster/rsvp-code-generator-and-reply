simple PHP script for generating unique key for user and interface for user to respond and keep track of users responded RSVP along with number of guests bringing. This is no way a secure script, there is protection against some SQL injections but nothing to prevent a brute force against the 6 digit unique key. Adding more complexity than 6 digit key validation was not ideal for the recipients I created this for. feel free to fork and add anything more you need. Using foundation framework for responsive web.
[demo][1]
[demo admin page][2]

[1]: http://dswabster.kodingen.com/rsvp/
[2]: http://dswabster.kodingen.com/rsvp/admin/
